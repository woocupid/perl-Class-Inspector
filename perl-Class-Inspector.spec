Name:           perl-Class-Inspector
Version:        1.32
Release:        5
Summary:        Get information about a class and its structure

License:        GPL+ or Artistic
URL:            http://search.cpan.org/dist/Class-Inspector/
Source0:        https://cpan.metacpan.org/authors/id/P/PL/PLICEASE/Class-Inspector-%{version}.tar.gz

BuildArch:      noarch


BuildRequires:  make perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) perl(File::Spec) >= 0.80 perl(Test::More)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%{?perl_default_filter}


%description
Class::Inspector allows you to get information about a loaded class.
Most or all of this information can be found in other ways, but they aren't
always very friendly, and usually involve a relatively high level of Perl
wizardry, or strange and unusual looking code. Class::Inspector attempts to
provide an easier, more friendly interface to this information.


%package_help


%prep
%autosetup -n Class-Inspector-%{version} -p1


%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%make_build


%install
rm -rf %{buildroot}
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -type f -name '*.bs' -a -size 0 -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null ';'
%{_fixperms} %{buildroot}/*


%check
make test


%files
%license LICENSE
%{perl_vendorlib}/*


%files help
%doc Changes README
%{_mandir}/man3/*.3*


%changelog
* Thu Nov 28 2019 Qianbiao.NG <Qianbiao.NG@turnbig.net> - 1.32-5
- Repackage for openEuler OS
